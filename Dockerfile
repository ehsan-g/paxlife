FROM python:3
# set a directory for the app
WORKDIR /usr/src/app

# copy all the files to the container
ADD requirements.txt /usr/src/app

# install dependencies
RUN pip install -r requirements.txt

ADD . /usr/src/app