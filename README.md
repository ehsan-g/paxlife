[![made-with-pythone](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/)
[![pipeline status](https://gitlab.com/ehsan-g/paxlife/badges/master/pipeline.svg)](https://gitlab.com/ehsan-g/paxlife/-/commits/master)
# PaxLife Project

The challenge is to write a small web service that provides
information about airports. You will have to construct the database
by yourself and the service should allow to query for airports by
IATA code as well as by text. The design of the endpoints is up to
your decision but both have to return JSON data.

#### API examples

- "users": "/api/users/",
- "groups": "/api/groups/",
- "airports": "/api/airports/"
- "Search IATA": "/api/airports/airport/iata/TGL"
- "Search Name": "/api/airports/airport/name/?search=berlin"


# Getting Started

These instructions will get you a copy of the project up and running
on your local machine for development and testing purposes. See
deployment for notes on how to deploy the project on a live 
system.

# Prerequisites

What things you need to install the software and how to install 
them on a linux or mac OS. 


## Setting Up

##### Step 1: To set up locally
Clone the below repository.
```shell script
    $ https://gitlab.com/ehsan-g/paxlife.git
```
##### Step 2: Install Pip package management system
```shell script
    $ sudo easy_install pip
```
##### Step 3: Install virtualenv for python.
```shell script
    $ sudo pip install virtualenv
    $ virtualenv <env_name>
    $ cd <env_name>
    $ source bin/activate
```
##### Step 4: Install the requirements
```shell script
    $ pip install -r requirements.txt
```

##### Step 5: Now start the project server
Fix the project database setting.
```
    DATABASES = {
        'default': {
            ...
            'HOST': 'localhost',
            ...
        }
    }
```
Run
```shell script
    $ python manage.py migrate
    $ python manage.py createsuperuser
    $ python manage.py runserver
```

### Alternative - DockerHub
A docker image could alternatively be provided to create a 
container and run the project from the container.Docker Hub as
 the registry.

##### Step 1: Setup Docker in your system
Please follow the document here to install [Docker].

##### Step 2: Fetch the Image
The pull command fetches the PaxLife image from the Docker 
registry and saves it to our system. 

###### ( not uploaded to DockerHub registry because it is public)
```shell script
    $ docker pull PaxLife 
```
##### Step 3: Let's now run a Docker container
For a custom port please use `-p` to which the client will forward
connections to the container.
```shell script
    $ docker run --rm <registry> echo "hello from Docker"
```

### Alternative - Create your image
Alternatively docker image can be created as follows:

##### Step 1: Change the project setting
```
    DATABASES = {
        'default': {
            ...
            'HOST': 'db',
            ...
        }
    }
```
##### Step 2: Run
```shell script
    $ doker-compose up
```



# Running the tests
This project will be tested via GitLab CI for the following:
1. IATA uniqueness
2. Airport is not None when pk=iata
3. Case insensitive and wrong IATA
4. In name search the end result and type of result


# Built With
- Python3 - Django Rest Framework
- PostgreSQL
- CI/CD: GitLab CI / Docker

# Features
- Build a Database with Airports and pk=iata.
- A CSV file can be uploaded.
- Raw CSV rows with no IATA will not be stored.
- One Endpoint for IATA Code search.
- One Endpoint for Airport Name search.
- Creating / Updating models using Serializer and Rest Framework.
- Pagination for provided Json.
- API routing.
- API: Users, Groups, Airports, SearchByName and SearchByIATA.


# Versioning
I will use [Semantic] Versioning.

# Contributing
- Fork it!
- Create your feature branch: 
```
$ git checkout -b my-new-feature
```
- Commit your changes: 
```
$ git commit -am 'Add some feature'

```
- Push to the branch: 
```
$ git push origin my-new-feature
```
- Submit a pull request


# Acknowledgements
**PaxLife** © 2020+. Released under the [PaxLife] License.<br>
Authored and maintained by Ehsan Ghasemi.



“One small step for man, one giant leap for mankind.” 
- [X] Receive the necessary file.
- [x] Create a README.md file.
- [x] Create a requirements.txt file.
- [x] Setup Django Project Setting and the Environment.
- [X] Build the models.
- [X] Import the CSV to the table.
- [X] Setup the Admin, URLs and View of the app.
- [X] Setup the Testing.
- [X] Setup the Dockerfile and DockerCompose.
- [X] Setup a GitLab CI / CD.
- [X] Submit.


###### Optional
- [X] Adding more features.
- [ ] Build a user friendly front-end.
- [ ] Multiple use of db instead of changing Database setting


> GitHub [@ehsan-g](https://github.com/ehsan-g/PaxLife) &nbsp;&middot;&nbsp;

>[PaxLife]: https://www.paxlife.aero/
>[Semantic]: https://semver.org/
>[Docker]: https://docs.docker.com/install/linux/docker-ce/ubuntu/
