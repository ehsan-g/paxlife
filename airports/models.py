from django.db import models
from django.contrib.postgres.fields import CITextField


# Create db.
class Airport(models.Model):
    airport_id = models.IntegerField()
    ident = models.CharField(max_length=10, default='Not Found')
    the_type = models.CharField(max_length=30, default='Not Found')
    name = models.CharField(max_length=128, default='Not Found')
    latitude = models.DecimalField(max_digits=15, decimal_places=6)
    longitude = models.DecimalField(max_digits=15, decimal_places=6, null=True)
    elevation_ft = models.DecimalField(max_digits=15, decimal_places=6, null=True)
    continent = models.CharField(max_length=2, default='Not Found')
    country = models.CharField(max_length=2, default='Not Found')
    iso_region = models.CharField(max_length=11, default='Not Found')
    city = models.CharField(max_length=64, default='Not Found')
    scheduled_service = models.BooleanField(default=False)
    icao = models.CharField(max_length=4)
    """
    for Case insensitive fix the migration module after makemigrations command.
    Please follow the instruction here: https://bit.ly/2vgs3VP
    """
    iata = CITextField(max_length=3, primary_key=True)
    local_code = models.CharField(max_length=10, default='Not Found')
    home_link = models.URLField(default='Not Found')
    wikipedia_link = models.URLField(default='Not Found')
    keywords = models.CharField(max_length=200, default='')

    def __str__(self):
        return f"(The airport {self.iata} in {self.city} is Available)"



