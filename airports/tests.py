import unittest
from django.test import TestCase, Client
from .models import Airport


# Create your tests here.
class ModelsTestCase(TestCase):
    """ We first create the objects and use the same objects  which won't mess up our db, we test different conditions
    which we expect our program behave. Back end and front end both are tested here"""
    def setUp(self):

        # Create airport.
        self.a = Airport.objects.create(airport_id=12345, ident='OEZL', the_type='small_airport',
                                        name='Tegel', latitude=26.350000, longitude=26.350000,
                                        elevation_ft=1900.000000, continent='AS', country='SA',
                                        iso_region='SA-01', city='Berlin', scheduled_service=False,
                                        icao='OEZL', iata='TGL', local_code='', home_link='',
                                        wikipedia_link='', keywords='')

    # Test IATA uniqueness
    def test_iata_count(self):
        a = Airport.objects.filter(iata="TGL").count()
        self.assertEqual(a, 1)

    # Test airports exist
    def test_valid_airport(self):
        a = Airport.objects.get(iata="TGL")
        self.assertIsNotNone(a)

    # Client Testing
    def test_search_iata(self):
        c = Client()
        response = c.get('/api/airports/airport/iata/TGL')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['iata'], 'TGL')

        response = c.get('/api/airports/airport/iata/tgl')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data['iata'], 'TGL')

        response = c.get('/api/airports/airport/iata/xxx')
        self.assertEqual(response.status_code, 404)

    def test_search_name(self):
        c = Client()
        response = c.get('/api/airports/airport/name/?search=tegel')
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.data['results'], list)
        self.assertEqual(response.data['results'][0]['name'], "Tegel")


if __name__ == "__main__":
    unittest.main()