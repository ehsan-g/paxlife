import csv
import io
from airports.serializers import UserSerializer, GroupSerializer, AirportsSerializer
from django.http import Http404, HttpResponseRedirect, JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User, Group
from rest_framework import viewsets, generics
from django.contrib import messages
from django.shortcuts import render
from django.urls import reverse
from .models import Airport
from rest_framework import filters
from django.db import utils

# v1.0.0
# Create your views here


# API for all Users
class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


# API for all User's Groups
class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


# API for all airports
class AirportViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows airports to be viewed or edited.
    """
    queryset = Airport.objects.all()
    serializer_class = AirportsSerializer


# Generic Filtering - Search by IATA
class AirportIATAView(generics.RetrieveAPIView):
    """
    API endpoint that allows airports to be searched by IATA.
    """
    queryset = Airport.objects.all()
    serializer_class = AirportsSerializer


# Generic Filtering - Search by Name
class AirportNameView(generics.ListCreateAPIView):
    """
    API endpoint that allows airports to be searched by IATA.
    """
    search_fields = ['name']
    filter_backends = (filters.SearchFilter,)
    queryset = Airport.objects.all()
    serializer_class = AirportsSerializer


def login_view(request):
    username = request.POST["username"]
    password = request.POST["password"]
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return HttpResponseRedirect(reverse("index"))
    else:
        return render(request, "airports/login.html", {"message": "Invalid credentials."})


def logout_view(request):
    logout(request)
    return render(request, "airports/login.html", {"message": "Logged out."})


def index(request):
    if not request.user.is_authenticated:
        return render(request, "airports/login.html", {"message": None})
    context = {
        "airports": Airport.objects.all()
    }
    return render(request, "airports/index.html", context)


def airport(request, iata):
    try:
        airport = Airport.objects.get(pk=iata)
    except Airport.DoesNotExist:
        raise Http404("Airport does not exist")
    context = {
        "airport": airport,
    }
    return render(request, "airports/airport.html", context)


def importing(request):
    # declaring template
    template = "index.html"
    data = Airport.objects.all()
    prompt = {
        'order': 'Order of the CSV should be airport_id, ident, the_type, name, latitude, longitude,'
                 ' elevation, continent, country, region, city, scheduled_service, icao, iata, '
                 'local_code, home_link wikipe ,dia_link, Keywords,',
        'airports': data
    }
    # GET request returns the value of the data with the specified key.
    if request.method == "GET":
        return render(request, template, prompt)

    csv_file = request.FILES['file']

    # let's check if it is a csv file
    if not csv_file.name.endswith('.csv'):
        messages.error(request, 'THIS IS NOT A CSV FILE')

    data_set = csv_file.read().decode('UTF-8')
    # setup a stream which is when we loop through each line we are able to handle a data in a stream
    io_string = io.StringIO(data_set)
    next(io_string)
    for column in csv.reader(io_string):
        # Do not store the empty IATAs and Capitalize the letters
        if column[13] == '':
            continue
        else:
            column[13] == column[13].upper()

        # Change scheduled_service to boolean
        if column[11] == 'no':
            column[11] = False
        else:
            column[11] = True

        # Place 0 for empty elevation
        if column[6] == '':
            column[6] = 0

        # Check for duplicate IATA, continue if existed
        try:
             _, created = Airport.objects.update_or_create(
                airport_id=column[0],
                ident=column[1],
                the_type=column[2],
                name=column[3],
                latitude=column[4],
                longitude=column[5],
                elevation_ft=column[6],
                continent=column[7],
                country=column[8],
                iso_region=column[9],
                city=column[10],
                scheduled_service=column[11],
                icao=column[12],
                iata=column[13],
                local_code=column[14],
                home_link=column[15],
                wikipedia_link=column[16],
                keywords=column[17],)

        except utils.IntegrityError:
            continue

    return JsonResponse({})
