from django.urls import path, include
from . import views
from rest_framework import routers

# To get a link of api from routers
router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'airports', views.AirportViewSet)


urlpatterns = [
    path("", views.index, name='index'),
    path("login", views.login_view, name="login"),
    path("logout", views.logout_view, name="logout"),
    path('airports/airport/<slug:iata>', views.airport, name='airport'),
    path("uploaded", views.importing, name="import"),
    # slug is type for char
    path('api/airports/airport/iata/<slug:pk>', views.AirportIATAView.as_view()), # Example http://127.0.0.1:8000/all/api/airports/airport/iata/?TGL
    path('api/airports/airport/name/', views.AirportNameView.as_view()), # Example http://127.0.0.1:8000/all/api/airports/airport/name/?search=berlin
    # Get a link of app apis
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

]