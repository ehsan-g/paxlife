from django.contrib.auth.models import User, Group
from rest_framework import serializers
from .models import Airport

# define some serializers, this will take the model and serialize it iand vice versa


# allow admin users to view and edit the users and groups in the system.
class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class AirportsSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Airport
        fields = ['name', 'iata', 'icao', 'city', 'country', 'latitude', 'longitude']
